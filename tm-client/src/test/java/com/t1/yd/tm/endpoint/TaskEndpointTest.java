package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.endpoint.IProjectEndpoint;
import com.t1.yd.tm.api.endpoint.ITaskEndpoint;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.dto.request.project.ProjectCreateRequest;
import com.t1.yd.tm.dto.request.task.*;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.exception.entity.TaskNotFoundException;
import com.t1.yd.tm.marker.IntegrationCategory;
import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.model.Task;
import com.t1.yd.tm.service.PropertyService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;

import java.util.List;

import static com.t1.yd.tm.constant.EndpointTestData.ADMIN_LOGIN;
import static com.t1.yd.tm.constant.EndpointTestData.ADMIN_PASSWORD;

@Category(IntegrationCategory.class)
public class TaskEndpointTest extends AbstractEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService);
    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Nullable
    private String token;
    @Nullable
    private Task testTask;

    @NotNull
    private Task createTask() {
        @NotNull final TaskCreateRequest createRequest = new TaskCreateRequest();
        createRequest.setToken(token);
        createRequest.setName("TASK1");
        createRequest.setDescription("Description");
        @Nullable final Task taskCreated = taskEndpoint.createTask(createRequest).getTask();
        Assert.assertNotNull(taskCreated);
        return taskCreated;
    }

    @NotNull
    private Project createProject() {
        @NotNull final ProjectCreateRequest createRequest = new ProjectCreateRequest();
        createRequest.setToken(token);
        createRequest.setName("PROJECT1");
        createRequest.setDescription("Description");
        @Nullable final Project projectCreated = projectEndpoint.createProject(createRequest).getProject();
        Assert.assertNotNull(projectCreated);
        return projectCreated;
    }

    @Nullable
    private Task findTaskById(final String Id) {
        @NotNull final TaskShowByIdRequest showByIdRequest = new TaskShowByIdRequest();
        showByIdRequest.setToken(token);
        showByIdRequest.setId(Id);
        return taskEndpoint.showTaskById(showByIdRequest).getTask();
    }

    @Nullable
    private Task findTaskByIndex(final Integer Index) {
        @NotNull final TaskShowByIndexRequest showByIndexRequest = new TaskShowByIndexRequest();
        showByIndexRequest.setToken(token);
        showByIndexRequest.setIndex(Index);
        return taskEndpoint.showTaskByIndex(showByIndexRequest).getTask();
    }

    @Before
    public void before() {
        token = login(ADMIN_LOGIN, ADMIN_PASSWORD);
        saveBackup(token);
        testTask = createTask();
    }

    @After
    public void after() {
        loadBackup(token);
        logout(token);
    }

    @Test
    public void changeTaskStatusById() {
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest();
        request.setToken(token);
        request.setId(testTask.getId());
        request.setStatus(Status.COMPLETED.toString());
        taskEndpoint.changeTaskStatusById(request);
        @Nullable Task taskFound = findTaskById(testTask.getId());
        Assert.assertEquals(taskFound.getStatus(), Status.COMPLETED);
    }

    @Test
    public void changeTaskStatusByIndex() {
        @NotNull final Status statusBefore = findTaskByIndex(0).getStatus();
        @NotNull final Status statusToSet = statusBefore == Status.COMPLETED ? Status.NOT_STARTED : Status.COMPLETED;
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest();
        request.setToken(token);
        request.setIndex(0);
        request.setStatus(statusToSet.toString());
        taskEndpoint.changeTaskStatusByIndex(request);
        @Nullable Task taskFound = findTaskByIndex(0);
        Assert.assertEquals(taskFound.getStatus(), statusToSet);
    }

    @Test
    public void clearTasks() {
        @NotNull final TaskClearRequest request = new TaskClearRequest();
        request.setToken(token);
        taskEndpoint.clearTasks(request);
        TaskNotFoundException taskNotFoundException = new TaskNotFoundException();
        thrown.expectMessage(taskNotFoundException.getMessage());
        Assert.assertNull(findTaskById(testTask.getId()));
    }

    @Test
    public void completeTaskById() {
        @NotNull final TaskChangeStatusByIdRequest changeRequest = new TaskChangeStatusByIdRequest();
        changeRequest.setToken(token);
        changeRequest.setId(testTask.getId());
        changeRequest.setStatus(Status.IN_PROGRESS.toString());
        taskEndpoint.changeTaskStatusById(changeRequest);
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest();
        request.setToken(token);
        request.setId(testTask.getId());
        taskEndpoint.completeTaskById(request);
        @Nullable final Task taskFound = findTaskById(testTask.getId());
        Assert.assertEquals(taskFound.getStatus(), Status.COMPLETED);
    }

    @Test
    public void completeTaskByIndex() {
        @NotNull final TaskChangeStatusByIndexRequest changeRequest = new TaskChangeStatusByIndexRequest();
        changeRequest.setToken(token);
        changeRequest.setIndex(0);
        changeRequest.setStatus(Status.IN_PROGRESS.toString());
        taskEndpoint.changeTaskStatusByIndex(changeRequest);
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest();
        request.setToken(token);
        request.setIndex(0);
        taskEndpoint.completeTaskByIndex(request);
        @Nullable final Task taskFound = findTaskByIndex(0);
        Assert.assertEquals(taskFound.getStatus(), Status.COMPLETED);
    }

    @Test
    public void listTasks() {
        @NotNull final TaskListRequest request = new TaskListRequest();
        request.setToken(token);
        request.setSort(Sort.BY_NAME.toString());
        @Nullable final List<Task> tasks = taskEndpoint.listTasks(request).getTasks();
        Assert.assertTrue(tasks.size() >= 1);
    }

    @Test
    public void removeTaskById() {
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest();
        request.setToken(token);
        request.setId(testTask.getId());
        taskEndpoint.removeTaskById(request);
        TaskNotFoundException taskNotFoundException = new TaskNotFoundException();
        thrown.expectMessage(taskNotFoundException.getMessage());
        Assert.assertNull(findTaskById(testTask.getId()));
    }

    @Test
    public void removeTaskByIndex() {
        @NotNull final Task taskFound = findTaskByIndex(0);
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest();
        request.setToken(token);
        request.setIndex(0);
        taskEndpoint.removeTaskByIndex(request);
        Assert.assertNotEquals(taskFound.getId(), findTaskByIndex(0).getId());
    }

    @Test
    public void startTaskById() {
        @NotNull final TaskChangeStatusByIdRequest changeRequest = new TaskChangeStatusByIdRequest();
        changeRequest.setToken(token);
        changeRequest.setId(testTask.getId());
        changeRequest.setStatus(Status.NOT_STARTED.toString());
        taskEndpoint.changeTaskStatusById(changeRequest);
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest();
        request.setToken(token);
        request.setId(testTask.getId());
        taskEndpoint.startTaskById(request);
        @Nullable final Task taskFound = findTaskById(testTask.getId());
        Assert.assertEquals(taskFound.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startTaskByIndex() {
        @NotNull final TaskChangeStatusByIndexRequest changeRequest = new TaskChangeStatusByIndexRequest();
        changeRequest.setToken(token);
        changeRequest.setIndex(0);
        changeRequest.setStatus(Status.NOT_STARTED.toString());
        taskEndpoint.changeTaskStatusByIndex(changeRequest);
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest();
        request.setToken(token);
        request.setIndex(0);
        taskEndpoint.startTaskByIndex(request);
        @Nullable final Task taskFound = findTaskByIndex(0);
        Assert.assertEquals(taskFound.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void updateTaskById() {
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest();
        request.setToken(token);
        request.setId(testTask.getId());
        request.setName(testTask.getName() + "1");
        request.setDescription(testTask.getDescription() + "1");
        taskEndpoint.updateTaskById(request);
        @Nullable final Task taskFound = findTaskById(testTask.getId());
        Assert.assertEquals(taskFound.getName(), testTask.getName() + "1");
        Assert.assertEquals(taskFound.getDescription(), testTask.getDescription() + "1");
    }

    @Test
    public void updateTaskByIndex() {
        @Nullable final Task taskToChange = findTaskByIndex(0);
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest();
        request.setToken(token);
        request.setIndex(0);
        request.setName(taskToChange.getName() + "1");
        request.setDescription(taskToChange.getDescription() + "1");
        taskEndpoint.updateTaskByIndex(request);
        @Nullable final Task taskFound = findTaskById(taskToChange.getId());
        Assert.assertEquals(taskFound.getName(), taskToChange.getName() + "1");
        Assert.assertEquals(taskFound.getDescription(), taskToChange.getDescription() + "1");
    }

    @Test
    public void bindUnbindTaskToProject() {
        @NotNull final Project testProject = createProject();
        @NotNull final TaskBindToProjectRequest bindRequest = new TaskBindToProjectRequest();
        bindRequest.setToken(token);
        bindRequest.setId(testTask.getId());
        bindRequest.setProjectId(testProject.getId());
        taskEndpoint.bindTaskToProject(bindRequest);
        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest();
        request.setToken(token);
        request.setProjectId(testProject.getId());
        @Nullable List<Task> tasks = taskEndpoint.listTasksByProjectId(request).getTasks();
        Assert.assertFalse(tasks.isEmpty());

        @NotNull final TaskUnbindFromProjectRequest unbindRequest = new TaskUnbindFromProjectRequest();
        unbindRequest.setToken(token);
        unbindRequest.setId(testTask.getId());
        unbindRequest.setProjectId(testProject.getId());
        taskEndpoint.unbindTaskFromProject(unbindRequest);
        tasks = taskEndpoint.listTasksByProjectId(request).getTasks();
        Assert.assertNull(tasks);
    }

}