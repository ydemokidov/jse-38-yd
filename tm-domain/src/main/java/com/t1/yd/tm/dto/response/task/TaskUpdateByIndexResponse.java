package com.t1.yd.tm.dto.response.task;

import com.t1.yd.tm.model.Task;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class TaskUpdateByIndexResponse extends AbstractTaskResponse {

    public TaskUpdateByIndexResponse(@NotNull final Task task) {
        super(task);
    }

    public TaskUpdateByIndexResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
