package com.t1.yd.tm.dto.response.project;

import com.t1.yd.tm.model.Project;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class ProjectCreateResponse extends AbstractProjectResponse {

    public ProjectCreateResponse(@Nullable Project project) {
        super(project);
    }

    public ProjectCreateResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
