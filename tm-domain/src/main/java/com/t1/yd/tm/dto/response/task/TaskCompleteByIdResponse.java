package com.t1.yd.tm.dto.response.task;

import com.t1.yd.tm.model.Task;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class TaskCompleteByIdResponse extends AbstractTaskResponse {

    public TaskCompleteByIdResponse(@Nullable final Task task) {
        super(task);
    }

    public TaskCompleteByIdResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
