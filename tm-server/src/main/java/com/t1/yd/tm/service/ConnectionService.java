package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.IPropertyService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.DriverManager;

@RequiredArgsConstructor
public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection() {
        @NotNull final String username = propertyService.getDbUsername();
        @NotNull final String password = propertyService.getDbPassword();
        @NotNull final String connectString = propertyService.getDbServer();
        @NotNull final Connection connection = DriverManager.getConnection(connectString, username, password);
        connection.setAutoCommit(false);
        return connection;
    }
}
