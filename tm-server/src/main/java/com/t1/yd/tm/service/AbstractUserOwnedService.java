package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.IUserOwnedRepository;
import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.IUserOwnedService;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.IndexIncorrectException;
import com.t1.yd.tm.model.AbstractUserOwnedEntity;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<E extends AbstractUserOwnedEntity, R extends IUserOwnedRepository<E>> extends AbstractService<E, R> implements IUserOwnedService<E> {

    public AbstractUserOwnedService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    @SneakyThrows
    public E add(@NotNull final String userId, @NotNull final E entity) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final R repository = getRepository(connection);
            @NotNull final E result = repository.add(userId, entity);
            connection.commit();
            return result;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final R repository = getRepository(connection);
            repository.clear(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final R repository = getRepository(connection);
            return repository.findAll(userId);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator comparator) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final R repository = getRepository(connection);
            if (comparator == null) return findAll(userId);
            return repository.findAll(userId, comparator);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    public List<E> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            if (id.isEmpty()) throw new IdEmptyException();
            @NotNull final R repository = getRepository(connection);
            return repository.findOneById(userId, id);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            if (index < 0) throw new IndexIncorrectException();
            @NotNull final R repository = getRepository(connection);
            return repository.findOneByIndex(userId, index);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            if (id.isEmpty()) throw new IdEmptyException();
            @NotNull final R repository = getRepository(connection);
            @Nullable final E result = repository.removeById(userId, id);
            connection.commit();
            return result;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            if (index < 0) throw new IndexIncorrectException();
            @NotNull final R repository = getRepository(connection);
            @Nullable final E result = repository.removeByIndex(userId, index);
            connection.commit();
            return result;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            if (id.isEmpty()) throw new IdEmptyException();
            @NotNull final R repository = getRepository(connection);
            return repository.existsById(userId, id);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize(@NotNull final String userId) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final R repository = getRepository(connection);
            return repository.getSize(userId);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
