package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.repository.IUserOwnedRepository;
import com.t1.yd.tm.model.AbstractUserOwnedEntity;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<E extends AbstractUserOwnedEntity> extends AbstractRepository<E> implements IUserOwnedRepository<E> {

    public AbstractUserOwnedRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    @NotNull
    public abstract E add(@NotNull final String userId, @NotNull final E entity);

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE user_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.executeUpdate();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@Nullable final String userId) {
        @NotNull final List<E> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE user_id = ?", getTableName());
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator comparator) {
        @NotNull final List<E> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE user_id = ? ORDER BY %s", getTableName(), getOrderByFieldFromComparator(comparator));
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE user_id =? AND  id = ?", getTableName());
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final String sql = String.format("SELECT * FROM (SELECT row_number() over() as rnum,* FROM %s WHERE user_id = ?) t1 WHERE rnum = ?", getTableName());
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setInt(2, index);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final E entity = findOneById(userId, id);
        if (entity == null) return null;
        @NotNull final String sql = String.format("DELETE FROM %s WHERE user_id = ? AND id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, id);
            statement.executeUpdate();
        }
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final E entity = findOneByIndex(userId, index);
        if (entity == null) return null;
        @NotNull final String sql = String.format("DELETE FROM %s WHERE user_id = ? AND rnum = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setInt(2, index);
            statement.executeUpdate();
        }
        return entity;
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public int getSize(@NotNull final String userId) {
        return findAll(userId).size();
    }

}
