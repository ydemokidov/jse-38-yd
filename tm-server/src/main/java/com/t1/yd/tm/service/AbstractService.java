package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.IRepository;
import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.IService;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.IndexIncorrectException;
import com.t1.yd.tm.model.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.validation.constraints.Null;
import java.sql.Connection;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

@AllArgsConstructor
public abstract class AbstractService<E extends AbstractEntity, R extends IRepository<E>> implements IService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    protected abstract R getRepository(@NotNull final Connection connection);

    @NotNull
    @Override
    public List<E> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @NotNull
    @Override
    @SneakyThrows
    public E add(@NotNull final E entity) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final R repository = getRepository(connection);
            E result = repository.add(entity);
            connection.commit();
            return result;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final R repository = getRepository(connection);
            repository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll() {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final R repository = getRepository(connection);
            return repository.findAll();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@Nullable final Comparator comparator) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final R repository = getRepository(connection);
            if (comparator == null) return findAll();
            return repository.findAll(comparator);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneById(@NotNull final String id) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            if (id.isEmpty()) throw new IdEmptyException();
            @NotNull final R repository = getRepository(connection);
            return repository.findOneById(id);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneByIndex(@NotNull final Integer index) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            if (index < 0) throw new IndexIncorrectException();
            @NotNull final R repository = getRepository(connection);
            return repository.findOneByIndex(index);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeById(@NotNull final String id) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            if (id.isEmpty()) throw new IdEmptyException();
            @NotNull final R repository = getRepository(connection);
            @Nullable final E result = repository.removeById(id);
            connection.commit();
            return result;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeByIndex(@NotNull final Integer index) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            if (index < 0) throw new IndexIncorrectException();
            @NotNull final R repository = getRepository(connection);
            @Nullable final E result = repository.removeByIndex(index);
            connection.commit();
            return result;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@NotNull final String id) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            if (id.isEmpty()) throw new IdEmptyException();
            @NotNull final R repository = getRepository(connection);
            return repository.existsById(id);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize() {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final R repository = getRepository(connection);
            return repository.getSize();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public @NotNull Collection<E> set(@NotNull Collection<E> collection) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final R repository = getRepository(connection);
            @NotNull final Collection<E> result = repository.set(collection);
            connection.commit();
            return result;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public @NotNull Collection<E> add(@NotNull Collection<E> collection) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final R repository = getRepository(connection);
            @NotNull final Collection<E> result = repository.add(collection);
            connection.commit();
            return result;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}