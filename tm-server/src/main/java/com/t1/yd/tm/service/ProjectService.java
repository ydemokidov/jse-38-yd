package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.IProjectRepository;
import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.IProjectService;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.exception.entity.ProjectNotFoundException;
import com.t1.yd.tm.exception.field.*;
import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.repository.ProjectRepository;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.sql.Connection;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    public Project findProjectById(@NotNull String id) {
        @Nullable final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @NotNull
    public Project findProjectByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @NotNull
    @Override
    public Project findProjectById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        if (name.isEmpty()) throw new NameEmptyException();
        if (description.isEmpty()) throw new DescriptionEmptyException();
        return add(new Project(userId, name, description));
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateById(@NotNull final String userId, @NotNull final String id, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();

        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository repository = getRepository(connection);
        try {
            @Nullable final Project project = repository.findOneById(userId, id);
            if (project == null) throw new ProjectNotFoundException();

            project.setName(name);
            project.setDescription(description);
            @NotNull final Project result = repository.update(project);

            connection.commit();
            return result;
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index < 0) throw new IndexIncorrectException();
        if (name.isEmpty()) throw new NameEmptyException();

        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository repository = getRepository(connection);
        try {
            @Nullable final Project project = repository.findOneByIndex(userId, index);
            if (project == null) throw new ProjectNotFoundException();

            project.setName(name);
            project.setDescription(description);
            Project result = repository.update(project);

            connection.commit();
            return result;
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();

        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository repository = getRepository(connection);
        try {
            @Nullable final Project project = repository.findOneById(userId, id);
            if (project == null) throw new ProjectNotFoundException();

            project.setStatus(status);

            @NotNull final Project result = repository.update(project);
            connection.commit();
            return result;
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository repository = getRepository(connection);
        if (index < 0 || index > repository.findAll(userId).size()) throw new IndexIncorrectException();
        try {
            @Nullable final Project project = repository.findOneByIndex(userId, index);
            if (project == null) throw new ProjectNotFoundException();

            project.setStatus(status);
            @NotNull final Project result = repository.update(project);

            connection.commit();
            return result;
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Project removeProjectById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();

        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository repository = getRepository(connection);
        try {
            @Nullable Project project = repository.removeById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            connection.commit();
            return project;
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Project removeProjectByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository repository = getRepository(connection);
        try {
            @Nullable final Project project = repository.removeByIndex(userId, index);
            if (project == null) throw new ProjectNotFoundException();
            connection.commit();
            return project;
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    protected IProjectRepository getRepository(@NotNull final Connection connection) {
        return new ProjectRepository(connection);
    }

    @Override
    public Connection getConnection() {
        throw new NotImplementedException();
    }

}