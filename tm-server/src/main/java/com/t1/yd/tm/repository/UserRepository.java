package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.repository.IUserRepository;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.model.User;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    private final String tableName = "users";

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password) {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        return user;
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password, @NotNull final String email) {
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password, @NotNull final String email, @NotNull final Role role) {
        @NotNull final User user = create(login, password, email);
        user.setRole(role);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE login = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE email = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        return findByEmail(email) != null;
    }

    @Override
    protected String getTableName() {
        return tableName;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User add(@NotNull final User entity) {
        @NotNull final String sql = String.format("INSERT INTO %s (id,login,pwd_hash,email,fst_name,last_name,mid_name,role,locked) VALUES (?,?,?,?,?,?,?,?,?)", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, entity.getId());
            statement.setString(2, entity.getLogin());
            statement.setString(3, entity.getPasswordHash());
            statement.setString(4, entity.getEmail());
            statement.setString(5, entity.getFirstName());
            statement.setString(6, entity.getLastName());
            statement.setString(7, entity.getMiddleName());
            statement.setString(8, entity.getRole().toString());
            statement.setBoolean(9, entity.getLocked());
            statement.executeUpdate();

            return entity;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    protected User fetch(@NotNull final ResultSet row) {
        @NotNull final User user = new User();

        user.setId(row.getString("id"));
        user.setLogin(row.getString("login"));
        user.setPasswordHash(row.getString("pwd_hash"));
        user.setEmail(row.getString("email"));
        user.setFirstName(row.getString("fst_name"));
        user.setLastName(row.getString("last_name"));
        user.setMiddleName(row.getString("mid_name"));
        user.setRole(Role.valueOf(row.getString("role")));
        user.setLocked(row.getBoolean("locked"));

        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User update(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET login = ?, pwd_hash = ?, email = ?, fst_name = ?, last_name = ?, mid_name = ?, role = ?, locked = ? where id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPasswordHash());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getFirstName());
            statement.setString(5, user.getLastName());
            statement.setString(6, user.getMiddleName());
            statement.setString(7, user.getRole().name());
            statement.setBoolean(8, user.getLocked());
            statement.setString(9, user.getId());
            statement.executeUpdate();
        }
        return user;
    }

    @Override
    public Connection getConnection() {
        return connection;
    }

}
