package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.repository.IProjectRepository;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.model.Project;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    private final String tableName = "projects";

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project add(@NotNull final String userId, @NotNull final Project entity) {
        @NotNull final String sql = String.format("INSERT INTO %s (id,name,description,created,status,user_id) VALUES (?,?,?,?,?,?)", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, entity.getId());
            statement.setString(2, entity.getName());
            statement.setString(3, entity.getDescription());
            statement.setTimestamp(4, new Timestamp(entity.getCreated().getTime()));
            statement.setString(5, entity.getStatus().getDisplayName());
            statement.setString(6, userId);
            statement.executeUpdate();

            return entity;
        }
    }

    @NotNull
    @Override
    protected String getTableName() {
        return tableName;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project add(@NotNull final Project entity) {
        @NotNull final String sql = String.format("INSERT INTO %s (id,name,description,created,status) VALUES (?,?,?,?,?)", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, entity.getId());
            statement.setString(2, entity.getName());
            statement.setString(3, entity.getDescription());
            statement.setTimestamp(4, new Timestamp(entity.getCreated().getTime()));
            statement.setString(5, entity.getStatus().getDisplayName());
            statement.executeUpdate();

            return entity;
        }
    }

    @Override
    @SneakyThrows
    protected Project fetch(@NotNull final ResultSet row) {
        @NotNull final Project project = new Project();
        project.setId(row.getString("id"));
        project.setCreated(row.getTimestamp("created"));
        project.setUserId(row.getString("user_id"));
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        @Nullable final Status status = Status.toStatus(row.getString("status"));
        project.setStatus(status == null ? Status.NOT_STARTED : status);

        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project update(@NotNull final Project project) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET user_id = ?, name = ?, description = ?, status = ? WHERE id = ?",
                getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getUserId());
            statement.setString(2, project.getName());
            statement.setString(3, project.getDescription());
            statement.setString(4, project.getStatus().getDisplayName());
            statement.setString(5, project.getId());
            statement.executeUpdate();
        }
        return project;
    }

}
