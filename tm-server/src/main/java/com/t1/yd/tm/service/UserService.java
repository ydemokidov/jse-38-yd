package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.IProjectRepository;
import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.api.repository.IUserRepository;
import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.ISaltProvider;
import com.t1.yd.tm.api.service.IUserService;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.exception.entity.UserNotFoundException;
import com.t1.yd.tm.exception.field.EmailEmptyException;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.LoginEmptyException;
import com.t1.yd.tm.exception.field.PasswordEmptyException;
import com.t1.yd.tm.exception.user.IsEmailExistException;
import com.t1.yd.tm.exception.user.IsLoginExistException;
import com.t1.yd.tm.model.User;
import com.t1.yd.tm.repository.ProjectRepository;
import com.t1.yd.tm.repository.TaskRepository;
import com.t1.yd.tm.repository.UserRepository;
import com.t1.yd.tm.util.HashUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.util.Objects;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final ISaltProvider saltProvider;

    public UserService(@NotNull final IConnectionService connectionService, @NotNull final ISaltProvider saltProvider) {
        super(connectionService);
        this.saltProvider = saltProvider;
    }

    @NotNull
    private IProjectRepository getProjectRepository(@NotNull final Connection connection) {
        return new ProjectRepository(connection);
    }

    @NotNull
    private ITaskRepository getTaskRepository(@NotNull final Connection connection) {
        return new TaskRepository(connection);
    }

    @NotNull
    @Override
    protected IUserRepository getRepository(@NotNull final Connection connection) {
        return new UserRepository(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User removeByLogin(@NotNull final String login) {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        try {
            @Nullable final User user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();

            @NotNull final User result = repository.removeById(user.getId());
            connection.commit();
            return result;
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User removeByEmail(@NotNull final String email) {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        try {
            @Nullable final User user = repository.findByEmail(email);
            if (user == null) throw new UserNotFoundException();

            @NotNull final User result = repository.removeById(user.getId());
            connection.commit();
            return result;
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    public User remove(@NotNull final User userToRemove) {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        try {
            @Nullable final User user = repository.findOneById(userToRemove.getId());
            if (user == null) throw new UserNotFoundException();

            @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
            taskRepository.clear(userToRemove.getId());

            @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
            projectRepository.clear(userToRemove.getId());

            connection.commit();
            return user;
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setPassword(@NotNull final String id, @NotNull final String password) {
        if (id.isEmpty()) throw new IdEmptyException();
        if (password.isEmpty()) throw new PasswordEmptyException();

        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);

        try {
            @Nullable final User user = repository.findOneById(id);
            if (user == null) throw new UserNotFoundException();
            user.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password, saltProvider)));
            @NotNull final User result = repository.update(user);
            connection.commit();
            return result;
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateUser(@NotNull final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName) {
        if (id.isEmpty()) throw new IdEmptyException();

        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        try {
            @Nullable final User user = repository.findOneById(id);
            if (user == null) throw new UserNotFoundException();

            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);

            @NotNull final User result = repository.update(user);

            connection.commit();
            return result;
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (password.isEmpty()) throw new PasswordEmptyException();

        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        if (repository.isLoginExist(login)) throw new IsLoginExistException();

        try {
            @NotNull final User user = repository.create(login, Objects.requireNonNull(HashUtil.salt(password, saltProvider)));
            connection.commit();
            return user;
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@NotNull final String login, @NotNull final String password, @NotNull final String email) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        if (email.isEmpty()) throw new EmailEmptyException();

        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        if (repository.isEmailExist(email)) throw new IsEmailExistException();
        if (repository.isLoginExist(login)) throw new IsLoginExistException();

        try {
            @NotNull final User user = repository.create(login, Objects.requireNonNull(HashUtil.salt(password, saltProvider)), email);
            connection.commit();
            return user;
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@NotNull final String login, @NotNull final String password, @NotNull final String email, @Nullable Role role) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) role = Role.USUAL;

        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        if (repository.isLoginExist(login)) throw new IsLoginExistException();

        try {
            @NotNull final User user = repository.create(login, Objects.requireNonNull(HashUtil.salt(password, saltProvider)), email, role);
            connection.commit();
            return user;
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public User findByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new LoginEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        @Nullable final User user = repository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @NotNull
    @Override
    public User findByEmail(@NotNull final String email) {
        if (email.isEmpty()) throw new EmailEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        @Nullable final User user = repository.findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        if (login.isEmpty()) throw new LoginEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        return repository.isLoginExist(login);
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        if (email.isEmpty()) throw new EmailEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        return repository.isEmailExist(email);
    }

    @Override
    @SneakyThrows
    public void lockByLogin(@NotNull final String login) {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        try {
            @Nullable final User user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(true);
            repository.update(user);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void unlockByLogin(@NotNull final String login) {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        try {
            @Nullable final User user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(false);
            repository.update(user);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}