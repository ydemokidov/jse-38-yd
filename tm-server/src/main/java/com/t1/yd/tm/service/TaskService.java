package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.ITaskService;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.exception.entity.TaskNotFoundException;
import com.t1.yd.tm.exception.field.*;
import com.t1.yd.tm.model.Task;
import com.t1.yd.tm.repository.TaskRepository;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.sql.Connection;
import java.util.List;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    public Task findTaskById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @NotNull
    @Override
    public Task findTaskByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @NotNull
    @Override
    public Task removeTaskById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Task task = removeById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @NotNull
    @Override
    public Task removeTaskByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Task task = removeByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        if (name.isEmpty()) throw new NameEmptyException();
        if (description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Task task = new Task(userId, name, description);
        return add(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateById(@NotNull final String userId, @NotNull final String id, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();

        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository repository = getRepository(connection);
        try {
            @Nullable final Task task = repository.findOneById(userId, id);
            if (task == null) throw new TaskNotFoundException();

            task.setName(name);
            task.setDescription(description);
            @NotNull final Task result = repository.update(task);

            connection.commit();
            return result;
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index < 0) throw new IndexIncorrectException();
        if (name.isEmpty()) throw new NameEmptyException();

        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository repository = getRepository(connection);
        try {
            @Nullable final Task task = repository.findOneByIndex(userId, index);
            if (task == null) throw new TaskNotFoundException();

            task.setName(name);
            task.setDescription(description);
            @NotNull final Task result = repository.update(task);

            connection.commit();
            return result;
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();

        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository repository = getRepository(connection);
        try {
            @Nullable final Task task = repository.findOneById(userId, id);
            if (task == null) throw new TaskNotFoundException();

            task.setStatus(status);
            @NotNull final Task result = repository.update(task);

            connection.commit();
            return result;
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository repository = getRepository(connection);
        if (index < 0 || index > repository.findAll(userId).size()) throw new IndexIncorrectException();
        try {
            @Nullable final Task task = repository.findOneByIndex(userId, index);
            if (task == null) throw new TaskNotFoundException();

            task.setStatus(status);
            @NotNull final Task result = repository.update(task);

            connection.commit();
            return result;
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = getRepository(connection);
            return repository.findAllByProjectId(userId, projectId);
        }
    }

    @NotNull
    @Override
    protected ITaskRepository getRepository(@NotNull final Connection connection) {
        return new TaskRepository(connection);
    }

    @Override
    public Connection getConnection() {
        throw new NotImplementedException();
    }

}