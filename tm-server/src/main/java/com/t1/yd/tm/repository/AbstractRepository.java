package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.repository.IRepository;
import com.t1.yd.tm.comparator.CreatedComparator;
import com.t1.yd.tm.comparator.NameComparator;
import com.t1.yd.tm.comparator.StatusComparator;
import com.t1.yd.tm.model.AbstractEntity;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    public Connection getConnection() {
        return connection;
    }

    protected abstract String getTableName();

    @NotNull
    @Override
    public abstract E add(@NotNull final E entity);

    @NotNull
    @Override
    public Collection<E> set(@NotNull Collection<E> collection) {
        clear();
        return add(collection);
    }

    @NotNull
    @Override
    public Collection<E> add(@NotNull Collection<E> collection) {
        @NotNull final List<E> result = new ArrayList<>();
        for (@NotNull final E entity : collection) {
            result.add(add(entity));
        }
        return result;
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String sql = String.format("DELETE FROM %s", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.executeUpdate();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll() {
        @NotNull final List<E> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s", getTableName());
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            @NotNull ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@NotNull final Comparator comparator) {
        @NotNull final List<E> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s ORDER BY %s", getTableName(), getOrderByFieldFromComparator(comparator));
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            @NotNull ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneById(final @NotNull String id) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE id = ?", getTableName());
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneByIndex(final @NotNull Integer index) {
        @NotNull final String sql = String.format("SELECT * FROM (SELECT row_number() over() as rnum, * FROM %s) t1 WHERE rnum = ?", getTableName());
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, index);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeById(final @NotNull String id) {
        @Nullable final E entity = findOneById(id);
        if (entity == null) return null;
        @NotNull final String sql = String.format("DELETE FROM %s WHERE id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.executeUpdate();
        }
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeByIndex(@NotNull final Integer index) {
        @Nullable final E entity = findOneByIndex(index);
        if (entity == null) return null;
        removeById(entity.getId());
        return entity;
    }

    @Override
    public boolean existsById(final @NotNull String id) {
        return findOneById(id) != null;
    }

    @SneakyThrows
    public int getSize() {
        @NotNull final String sql = String.format("SELECT COUNT(1) FROM %s", getTableName());
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getInt(1);
        }
    }

    protected abstract E fetch(final ResultSet row);

    protected String getOrderByFieldFromComparator(Comparator comparator) {
        if (CreatedComparator.INSTANCE.equals(comparator)) return "created";
        if (StatusComparator.INSTANCE.equals(comparator)) return "status";
        if (NameComparator.INSTANCE.equals(comparator)) return "name";
        else return "id";
    }

}
