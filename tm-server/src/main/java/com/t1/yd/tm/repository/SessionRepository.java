package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.repository.ISessionRepository;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.model.Session;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    private final String tableName = "sessions";

    public SessionRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session add(@NotNull final String userId, @NotNull final Session entity) {
        @NotNull final String sql = String.format("INSERT INTO %s (id,user_id,date,role) VALUES (?,?,?,?)", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, entity.getId());
            statement.setString(2, userId);
            statement.setTimestamp(3, new Timestamp(entity.getDate().getTime()));
            statement.setString(4, entity.getRole().toString());
            statement.executeUpdate();
            return entity;
        }
    }

    @Override
    protected String getTableName() {
        return tableName;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session add(@NotNull final Session entity) {
        @NotNull final String sql = String.format("INSERT INTO %s (id,date,role) VALUES (?,?,?)", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, entity.getId());
            statement.setTimestamp(2, new Timestamp(entity.getDate().getTime()));
            statement.setString(3, entity.getRole().toString());
            statement.executeUpdate();
            return entity;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Session fetch(@NotNull final ResultSet row) {
        @NotNull final Session session = new Session();

        session.setId(row.getString("id"));
        session.setDate(row.getTimestamp("date"));
        session.setRole(Role.valueOf(row.getString("role")));
        session.setUserId(row.getString("user_id"));

        return session;
    }
}
