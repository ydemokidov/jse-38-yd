package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.repository.IProjectRepository;
import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.marker.UnitCategory;
import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.service.ConnectionService;
import com.t1.yd.tm.service.PropertyService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.t1.yd.tm.constant.ProjectTestData.*;
import static com.t1.yd.tm.constant.UserTestData.ADMIN;
import static com.t1.yd.tm.constant.UserTestData.USER1;

@Category(UnitCategory.class)
public class ProjectRepositoryTest {

    private final IPropertyService propertyService = new PropertyService();

    private final IConnectionService connectionService = new ConnectionService(propertyService);

    private IProjectRepository repository;

    @Before
    public void initRepository() {
        repository = new ProjectRepository(connectionService.getConnection());
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void add() {
        repository.add(USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1.getId(), repository.findOneById(USER1_PROJECT1.getId()).getId());
    }

    @Test
    public void addWithUser() {
        repository.add(USER1.getId(), USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1.getId(), repository.findOneById(USER1_PROJECT1.getId()).getId());
    }

    @Test
    public void addAll() {
        repository.add(ALL_PROJECTS);
        Assert.assertEquals(ALL_PROJECTS.size(), repository.findAll().size());
        Assert.assertEquals(USER1_PROJECT2.getId(), repository.findOneById(USER1_PROJECT2.getId()).getId());
    }

    @Test
    public void removeById() {
        repository.add(ALL_PROJECTS);
        repository.removeById(USER1_PROJECT2.getId());
        Assert.assertEquals(ALL_PROJECTS.size() - 1, repository.findAll().size());
        Assert.assertNull(repository.findOneById(USER1_PROJECT2.getId()));
    }

    @Test
    public void removeByIndex() {
        repository.add(ALL_PROJECTS);
        @Nullable final Project removedProject = repository.removeByIndex(1);
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(ALL_PROJECTS.size() - 1, repository.findAll().size());
        Assert.assertNotEquals(removedProject.getId(), repository.findOneByIndex(1).getId());
    }

    @Test
    public void clear() {
        repository.add(ALL_PROJECTS);
        repository.clear();
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void findAllByUserId() {
        repository.add(USER1.getId(), USER1_PROJECT1);
        repository.add(USER1.getId(), USER1_PROJECT2);
        repository.add(ADMIN.getId(), ADMIN_PROJECT1);
        Assert.assertEquals(USER1_PROJECTS.size(), repository.findAll(USER1.getId()).size());
    }

    @Test
    public void findOneByIdWithUserId() {
        repository.add(USER1.getId(), USER1_PROJECT1);
        repository.add(USER1.getId(), USER1_PROJECT2);
        repository.add(ADMIN.getId(), ADMIN_PROJECT1);

        Assert.assertEquals(USER1_PROJECT1.getId(), repository.findOneById(USER1.getId(), USER1_PROJECT1.getId()).getId());
        Assert.assertNull(repository.findOneById(ADMIN.getId(), USER1_PROJECT1.getId()));
    }

    @Test
    public void findOneByIndexWithUserIdPositive() {
        repository.add(USER1.getId(), USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1.getId(), repository.findOneByIndex(USER1.getId(), 1).getId());
    }

    @Test
    public void findOneByIndexWithUserIdNegative() {
        repository.add(USER1.getId(), USER1_PROJECT1);
        Assert.assertNull(repository.findOneByIndex(ADMIN.getId(), 0));
    }

    @Test
    public void existsById() {
        repository.add(USER1_PROJECT1);
        Assert.assertTrue(repository.existsById(USER1_PROJECT1.getId()));
        Assert.assertFalse(repository.existsById(USER1_PROJECT2.getId()));
    }

    @After
    @SneakyThrows
    public void clearData() {
        repository.clear();
        repository.getConnection().close();
    }

}
